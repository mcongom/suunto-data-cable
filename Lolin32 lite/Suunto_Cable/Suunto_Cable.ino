#include <SoftwareSerial.h>

#define SW_TX 4

SoftwareSerial mySerial(-1, SW_TX); // RX, TX

void setup(){
  Serial.begin(115200);     // Between computer and Arduino
  Serial2.begin(115200);    // Between Arduino and Diving Computer
  mySerial.begin(115200);   // Between Arduino and Diving Computer
  
  pinMode(SW_TX,INPUT_PULLUP);
}

void loop(){
  if(Serial.available() > 0) {
    pinMode(SW_TX,OUTPUT);  
    digitalWrite(SW_TX,HIGH); //UART lines should be HIGH on IDLE
  
      while(Serial.available()  > 0){
        mySerial.write(Serial.read());
      }
    pinMode(SW_TX,INPUT_PULLUP);
  }
  
  while(Serial2.available() > 0){
    Serial.write(Serial2.read());
  }
}
