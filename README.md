# Suunto Data Cable

DIY Suunto USB Download Data cable for Diving computers (Zoop Novo, D4, D6 etc..)

Read more here:
[Making a DIY data cable for the Zoop Novo Diving Computer](http://itay.cc/?p=340)

![Connector](Photos/Connector.png)

![Suunto DM5](http://itay.mobi/blog/wp-content/uploads/2019/01/ZOOP-APP-1024x778.png)